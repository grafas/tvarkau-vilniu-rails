FactoryGirl.define do
  factory :report do
    ref_no "MyString"
    description "MyString"
    address "MyString"
    lat "9.99"
    lng "9.99"
    report_date "2017-02-24 22:17:25"
  end
end
